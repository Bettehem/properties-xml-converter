package tool;

public interface ConversionProgressListener {
    void onProgressUpdate(String message);
    void onConversionFinished(String message);
}
