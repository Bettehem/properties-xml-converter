package tool;

public interface DetailedConversionProgressListener extends ConversionProgressListener {
    void onDetailedProgressUpdate(String message);
}
