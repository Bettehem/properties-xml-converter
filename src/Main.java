import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tool.ConversionProgressListener;
import tool.DetailedConversionProgressListener;
import tool.FileConverter;

import java.io.File;
import java.util.Arrays;

public class Main extends Application implements EventHandler<ActionEvent>, DetailedConversionProgressListener{

    //For the base ui
    private Stage window;
    private Scene scene;
    private VBox layout;

    //Label in javaFX is similar to TextView in Android
    private Label enterPathTextView;
    private Label fileConversionStatus;
    private Label detailedFileConversionStatus;

    //TextField in javaFX is similar to EditText in Android
    private TextField filePathEditText;

    private Button browseFileButton;
    private Button convertButton;

    private static  String inputFilePath;
    private static String outputFilePath;
    private static String useInputFileDirectory;

    public static void main(String[] args){
        //check if arguments for input and output files are provided
        if (args.length == 4){
            //check first argument
            switch (args[0]){
                //if an input file path is provided
                case "-i":
                    inputFilePath = args[1];
                    break;

                //if an output file path is provided
                case "-o":
                    outputFilePath = args[1];
                    break;
            }

            //check second argument
            switch (args[2]){
                //if an input file path is provided
                case "-i":
                    inputFilePath = args[3];
                    break;

                //if an output file path is provided
                case "-o":
                    outputFilePath = args[3];
                    break;
            }

            FileConverter.convert(new ConversionProgressListener() {
                @Override
                public void onProgressUpdate(String message) {
                    System.out.println(message);
                }

                @Override
                public void onConversionFinished(String message) {
                    System.out.println(message);
                    System.exit(0);
                }
            }, inputFilePath, outputFilePath);

        }else{
            launch(args);
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        variables();
        ui();
        window.show();
    }

    private void variables(){
        textViews();
        editTexts();
        buttons();
    }

    private void textViews(){
        enterPathTextView = new Label();
        enterPathTextView.setText("Enter path to File that you want to convert or click on browse:");

        fileConversionStatus = new Label();
        detailedFileConversionStatus = new Label();
    }

    private void editTexts(){
        filePathEditText = new TextField();
    }

    private void buttons(){
        browseFileButton = new Button();
        browseFileButton.setText("Browse File");
        browseFileButton.setOnAction(this);

        convertButton = new Button();
        convertButton.setText("Convert");
        convertButton.setOnAction(this);
    }



    private void ui(){
        layouts();
        scenes();
        stages();
    }

    //handles what items are displayed and how they are displayed
    private void layouts(){
        layout = new VBox(10);
        layout.getChildren().addAll(enterPathTextView, filePathEditText, browseFileButton, convertButton, fileConversionStatus, detailedFileConversionStatus);
    }

    private void scenes(){
        scene = new Scene(layout, 640, 360);
    }

    private void stages(){
        window.setTitle("Java Properties - strings.xml converter");
        window.setScene(scene);
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource() == convertButton){
            FileConverter.convert(this, filePathEditText.getText());

        }else if (event.getSource() == browseFileButton){
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose file to convert");
            fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("File to convert", Arrays.asList(new String[]{"*.properties", "*.xml"})));
            File file = fileChooser.showOpenDialog(window);
            if (file != null){
                filePathEditText.setText(file.getAbsolutePath());
            }
        }
    }

    @Override
    public void onProgressUpdate(String message) {
        fileConversionStatus.setText(message);
    }

    @Override
    public void onConversionFinished(String message) {
        fileConversionStatus.setText(message);
    }

    @Override
    public void onDetailedProgressUpdate(String message) {
        detailedFileConversionStatus.setText(message);

    }
}
