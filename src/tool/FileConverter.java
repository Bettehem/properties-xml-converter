package tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;

public final class FileConverter {
    private static final String CONVERTER_VERSION = "1.4";
    private static ConversionProgressListener conversionProgressListener;
    private static DetailedConversionProgressListener detailedConversionProgressListener;

    public static void convert(ConversionProgressListener conversionProgressListener, String filePath){
        //check for commands
        switch (filePath){
            case "#version":
                conversionProgressListener.onProgressUpdate("Converter version: " + CONVERTER_VERSION);
                break;
            case "#clear":
                conversionProgressListener.onProgressUpdate("");
                break;
            default:
                FileConverter.conversionProgressListener = conversionProgressListener;

                conversionProgressListener.onProgressUpdate("Checking if file exists...");
                if (fileExists(filePath)){

                    conversionProgressListener.onProgressUpdate("Checking if file is valid...");
                    if (isFile(filePath)){
                        File file = new File(filePath);

                        conversionProgressListener.onProgressUpdate("Checking if file can be converted...");
                        if (isConvertible(file)){
                            conversionProgressListener.onProgressUpdate("Checking if file is a .properties file or .xml file...");
                            if (file.getAbsolutePath().endsWith(".properties")){
                                conversionToXml(file);
                            }else if (file.getAbsolutePath().endsWith(".xml")){
                                conversionToProperties(file);
                            }
                        }else{
                            conversionProgressListener.onProgressUpdate("This file type can't be converted!");
                        }
                    }else{
                        conversionProgressListener.onProgressUpdate("Please enter a file in to the path!");
                    }
                }else{
                    conversionProgressListener.onProgressUpdate("File not Found! Check your path and try again.");
                }
                break;
        }
    }

    public static void convert(DetailedConversionProgressListener detailedConversionProgressListener, String filePath){

        switch (filePath){
            case "#version":
                detailedConversionProgressListener.onProgressUpdate("Converter version: " + CONVERTER_VERSION);
                break;
            case "#clear":
                detailedConversionProgressListener.onDetailedProgressUpdate("");
                detailedConversionProgressListener.onProgressUpdate("");
                break;
            default:
                FileConverter.detailedConversionProgressListener = detailedConversionProgressListener;

                detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file exists...");
                if (fileExists(filePath)){

                    detailedConversionProgressListener.onProgressUpdate("Checking if file is valid...");
                    if (isFile(filePath)){
                        File file = new File(filePath);

                        detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file can be converted...");
                        if (isConvertible(file)){
                            detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file is a .properties file or .xml file...");
                            if (file.getAbsolutePath().endsWith(".properties")){
                                detailedConversionToXml(file);
                            }else if (file.getAbsolutePath().endsWith(".xml")){
                                detailedConversionToProperties(file);
                            }
                        }else{
                            detailedConversionProgressListener.onProgressUpdate("This file type can't be converted!");
                            detailedConversionProgressListener.onDetailedProgressUpdate("");
                        }
                    }else{
                        detailedConversionProgressListener.onProgressUpdate("Please enter a file in to the path!");
                        detailedConversionProgressListener.onDetailedProgressUpdate("");
                    }
                }else{
                    detailedConversionProgressListener.onProgressUpdate("File not Found! Check your path and try again.");
                    detailedConversionProgressListener.onDetailedProgressUpdate("");
                }
                break;
        }
    }

    public static void convert(ConversionProgressListener conversionProgressListener, String inputFile, String outputFile){
        //check for commands
        switch (inputFile){
            case "#version":
                conversionProgressListener.onProgressUpdate("Converter version: " + CONVERTER_VERSION);
                break;
            case "#clear":
                conversionProgressListener.onProgressUpdate("");
                break;
            default:
                FileConverter.conversionProgressListener = conversionProgressListener;

                conversionProgressListener.onProgressUpdate("Checking if file exists...");
                if (fileExists(inputFile)){

                    conversionProgressListener.onProgressUpdate("Checking if file is valid...");
                    if (isFile(inputFile)){
                        File file = new File(inputFile);

                        conversionProgressListener.onProgressUpdate("Checking if file can be converted...");
                        if (isConvertible(file)){
                            conversionProgressListener.onProgressUpdate("Checking if file is a .properties file or .xml file...");
                            if (file.getAbsolutePath().endsWith(".properties")){
                                conversionToXml(file, new File(outputFile));
                            }else if (file.getAbsolutePath().endsWith(".xml")){
                                conversionToProperties(file, new File(outputFile));
                            }
                        }else{
                            conversionProgressListener.onProgressUpdate("This file type can't be converted!");
                        }
                    }else{
                        conversionProgressListener.onProgressUpdate("Please enter a file in to the path!");
                    }
                }else{
                    conversionProgressListener.onProgressUpdate("File not Found! Check your path and try again.");
                }
                break;
        }
    }

    public static void convert(DetailedConversionProgressListener detailedConversionProgressListener, String inputFile, String outputFile){

        switch (inputFile){
            case "#version":
                detailedConversionProgressListener.onProgressUpdate("Converter version: " + CONVERTER_VERSION);
                break;
            case "#clear":
                detailedConversionProgressListener.onDetailedProgressUpdate("");
                detailedConversionProgressListener.onProgressUpdate("");
                break;
            default:
                FileConverter.detailedConversionProgressListener = detailedConversionProgressListener;

                detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file exists...");
                if (fileExists(inputFile)){

                    detailedConversionProgressListener.onProgressUpdate("Checking if file is valid...");
                    if (isFile(inputFile)){
                        File file = new File(inputFile);

                        detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file can be converted...");
                        if (isConvertible(file)){
                            detailedConversionProgressListener.onDetailedProgressUpdate("Checking if file is a .properties file or .xml file...");
                            if (file.getAbsolutePath().endsWith(".properties")){
                                detailedConversionToXml(file, new File(outputFile));
                            }else if (file.getAbsolutePath().endsWith(".xml")){
                                detailedConversionToProperties(file, new File(outputFile));
                            }
                        }else{
                            detailedConversionProgressListener.onProgressUpdate("This file type can't be converted!");
                            detailedConversionProgressListener.onDetailedProgressUpdate("");
                        }
                    }else{
                        detailedConversionProgressListener.onProgressUpdate("Please enter a file in to the path!");
                        detailedConversionProgressListener.onDetailedProgressUpdate("");
                    }
                }else{
                    detailedConversionProgressListener.onProgressUpdate("File not Found! Check your path and try again.");
                    detailedConversionProgressListener.onDetailedProgressUpdate("");
                }
                break;
        }
    }

    private static boolean fileExists(String filePath){
        return new File(filePath).exists();
    }

    private static boolean isFile(String filePath){
        return new File(filePath).isFile();
    }

    private static boolean isConvertible(File file){
        return file.getAbsolutePath().endsWith(".properties") || file.getAbsolutePath().endsWith(".xml");
    }


    private static void conversionToXml(File file){


        conversionProgressListener.onProgressUpdate("Converting .properties file to strings.xml...");

        String fileStart = "<resources>";
        String lineStart = "    <string name=\"";
        String lineMiddle = "\">";
        String lineEnd = "</string>";
        String fileEnd = "</resources>";

        File convertedFile = new File(file.getParent() + File.separator + "strings.xml");

        Properties properties = new Properties();
        FileInputStream inputStream;
        String keysString = "";
        String[] keysArray;
        try {
            inputStream = new FileInputStream(file);
            properties.load(inputStream);


            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                if (keysString.contentEquals("")){
                    keysString = (String) e.nextElement();
                }else {
                    keysString = keysString + ";#;" + (String) e.nextElement();
                }
            }

            keysArray = keysString.split(";#;");


            ArrayList<String> linesToConvert = new ArrayList<String>();

            for (String s : keysArray){
                linesToConvert.add(s + "=" + properties.getProperty(s));
            }

            ArrayList<String> convertedLines = new ArrayList<String>();

            convertedLines.add(fileStart);

            for (String s : linesToConvert){
                String tmp = lineStart + s.split("=")[0].replace(".", "") + lineMiddle + s.split("=")[1] + lineEnd;
                convertedLines.add(tmp);
            }

            convertedLines.add(fileEnd);

            Files.write(convertedFile.toPath().toAbsolutePath(), convertedLines, StandardCharsets.UTF_8);

            conversionProgressListener.onProgressUpdate("File Converted! Saved as \"strings.xml\" at " + convertedFile.getAbsolutePath());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void conversionToProperties(File file){
        conversionProgressListener.onProgressUpdate("Converting .xml file to .properties");

        String xmlFileStart = "<resources>";
        String xmlLineStart = "    <string name=\"";
        String xmlLineMiddle = "\">";
        String xmlLineEnd = "</string>";
        String xmlFileEnd = "</resources>";

        File convertedFile = new File(file.getParent() + File.separator + "java.properties");

        try{
            Properties properties = new Properties();
            FileOutputStream outputStream = new FileOutputStream(convertedFile);


            List<String> linesToConvert =  Files.readAllLines(file.toPath().toAbsolutePath(), StandardCharsets.UTF_8);

            ArrayList<String> convertedLines = new ArrayList<String>();

            //TODO: Update to newest "parser"
            for (String s : linesToConvert){
                if (!s.contentEquals(xmlFileStart) && !s.contentEquals(xmlFileEnd)){
                    String tmp = s.replace(xmlFileStart, "").replace(xmlLineStart, "").replace(" translatable=\"false\"", "").replace("translatable=\"false\"", "").replace(xmlLineMiddle, "=").replace(xmlLineEnd, "").replace("    ", "").replace("\t", "");
                    convertedLines.add(tmp);
                }
            }

            for (String s : convertedLines){
                properties.setProperty(s.split("=")[0], s.split("=")[1]);
            }

            properties.store(outputStream, null);

            conversionProgressListener.onProgressUpdate("File Converted! Saved as \"java.properties\" at " + convertedFile.getAbsolutePath());

        }catch (Exception e){
            conversionProgressListener.onProgressUpdate("Something went wrong!");
        }
    }


    private static void detailedConversionToXml(File file){
        detailedConversionProgressListener.onProgressUpdate("Converting .properties file to strings.xml...");

        detailedConversionProgressListener.onDetailedProgressUpdate("Configuring tools...");
        String fileStart = "<resources>";
        String lineStart = "    <string name=\"";
        String lineMiddle = "\">";
        String lineEnd = "</string>";
        String fileEnd = "</resources>";

        detailedConversionProgressListener.onDetailedProgressUpdate("Creating new file with the name \"strings.xml\"...");
        File convertedFile = new File(file.getParent() + File.separator + "strings.xml");

        Properties properties = new Properties();
        FileInputStream inputStream;
        String keysString = "";
        String[] keysArray;
        try {
            inputStream = new FileInputStream(file);
            properties.load(inputStream);

            detailedConversionProgressListener.onDetailedProgressUpdate("Getting key names from properties file...");
            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                if (keysString.contentEquals("")){
                    keysString = (String) e.nextElement();
                }else {
                    keysString = keysString + ";#;" + (String) e.nextElement();
                }
            }

            keysArray = keysString.split(";#;");


            ArrayList<String> linesToConvert = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating lines that exist in the raw file.");
            for (String s : keysArray){
                linesToConvert.add(s + "=" + properties.getProperty(s));
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating list for converted lines...");
            ArrayList<String> convertedLines = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Adding start of file...");
            convertedLines.add(fileStart);

            detailedConversionProgressListener.onDetailedProgressUpdate("Converting lines...");
            for (String s : linesToConvert){
                String tmp = lineStart + s.split("=")[0].replace(".", "") + lineMiddle + s.split("=")[1] + lineEnd;
                convertedLines.add(tmp);
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Converted lines. Adding file ending...");
            convertedLines.add(fileEnd);

            detailedConversionProgressListener.onDetailedProgressUpdate("Writing new file...");
            Files.write(convertedFile.toPath().toAbsolutePath(), convertedLines, StandardCharsets.UTF_8);

            detailedConversionProgressListener.onProgressUpdate("File Converted! Saved as \"strings.xml\" at " + convertedFile.getAbsolutePath());
            detailedConversionProgressListener.onDetailedProgressUpdate("");
        }catch (Exception e){
            e.printStackTrace();
            detailedConversionProgressListener.onProgressUpdate("Something went wrong!");
            detailedConversionProgressListener.onDetailedProgressUpdate(e.getLocalizedMessage());
        }
    }

    private static void detailedConversionToProperties(File file){
        detailedConversionProgressListener.onProgressUpdate("Converting .xml file to .properties");

        String xmlFileStart = "<resources>";
        String xmlLineStart = "<string name=\"";
        String xmlLineMiddle = "\">";
        String xmlLineEnd = "</string>";
        String xmlFileEnd = "</resources>";

        detailedConversionProgressListener.onDetailedProgressUpdate("Creating new file with the name \"java.properties\"...");
        File convertedFile = new File(file.getParent() + File.separator + "java.properties");

        try{
            Properties properties = new Properties();
            FileOutputStream outputStream = new FileOutputStream(convertedFile);


            detailedConversionProgressListener.onDetailedProgressUpdate("Reading lines to convert...");
            List<String> linesToConvert =  Files.readAllLines(file.toPath().toAbsolutePath(), StandardCharsets.UTF_8);

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating list for converted lines...");
            ArrayList<String> convertedLines = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Converting lines...");
            boolean temp = false;
            for (String s : linesToConvert){
                if (!s.contentEquals(xmlFileStart) && !s.contentEquals(xmlFileEnd) && !s.contentEquals("") && !s.contains("translatable=\"false\">")){
                    String tmp;

                    if (s.startsWith("<?xml") || s.startsWith("<!--")){
                        temp = true;
                        tmp = s.replace("<?xml", "#").replace("<!--", "#");
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = tmp.replace("?>", "").replace("-->", "");
                        }
                    }else if (temp){
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = "#" + s;
                        }else{
                            temp = true;
                            tmp = "#" + s;
                        }
                    }else{
                        tmp = s.replace("    ", "").replace("\t", "").replace("  ", "").replace(xmlFileStart, "").replace(xmlLineStart, "").replace(xmlLineMiddle, "=").replace(xmlLineEnd, "");
                    }
                    convertedLines.add(tmp);
                }
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Writing new Properties...");
            for (String s : convertedLines){
                if (!s.startsWith("#")){
                    properties.setProperty(s.split("=")[0], s.split("=")[1]);
                }
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Saving new file...");
            properties.store(outputStream, null);

            detailedConversionProgressListener.onProgressUpdate("File Converted! Saved as \"java.properties\" at " + convertedFile.getAbsolutePath());
            detailedConversionProgressListener.onDetailedProgressUpdate("");

        }catch (Exception e){
            e.printStackTrace();
            detailedConversionProgressListener.onProgressUpdate("Something went wrong!");
            detailedConversionProgressListener.onDetailedProgressUpdate(e.getMessage());
        }
    }



    private static void conversionToXml(File inputFile, File outputFile){


        conversionProgressListener.onProgressUpdate("Converting .properties file to .xml...");

        String fileStart = "<resources>";
        String lineStart = "    <string name=\"";
        String lineMiddle = "\">";
        String lineEnd = "</string>";
        String fileEnd = "</resources>";

        File convertedFile = outputFile;

        Properties properties = new Properties();
        FileInputStream inputStream;
        String keysString = "";
        String[] keysArray;
        try {
            inputStream = new FileInputStream(inputFile);
            properties.load(inputStream);


            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                if (keysString.contentEquals("")){
                    keysString = (String) e.nextElement();
                }else {
                    keysString = keysString + ";#;" + (String) e.nextElement();
                }
            }

            keysArray = keysString.split(";#;");


            ArrayList<String> linesToConvert = new ArrayList<String>();

            for (String s : keysArray){
                linesToConvert.add(s + "=" + properties.getProperty(s));
            }

            ArrayList<String> convertedLines = new ArrayList<String>();

            convertedLines.add(fileStart);

            for (String s : linesToConvert){
                String tmp = lineStart + s.split("=")[0].replace(".", "") + lineMiddle + s.split("=")[1] + lineEnd;
                convertedLines.add(tmp);
            }

            convertedLines.add(fileEnd);

            Files.write(convertedFile.toPath().toAbsolutePath(), convertedLines, StandardCharsets.UTF_8);

            conversionProgressListener.onProgressUpdate("File Converted! Saved as \"" + convertedFile.getName() + "\" at " + convertedFile.getAbsolutePath());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void conversionToProperties(File inputFile, File outputFile){
        conversionProgressListener.onProgressUpdate("Converting .xml file to .properties");

        String xmlFileStart = "<resources>";
        String xmlLineStart = "<string name=\"";
        String xmlLineMiddle = "\">";
        String xmlLineEnd = "</string>";
        String xmlFileEnd = "</resources>";

        File convertedFile = outputFile;

        try{
            Properties properties = new Properties();
            FileOutputStream outputStream = new FileOutputStream(convertedFile);


            List<String> linesToConvert =  Files.readAllLines(inputFile.toPath().toAbsolutePath(), StandardCharsets.UTF_8);

            ArrayList<String> convertedLines = new ArrayList<String>();

            boolean temp = false;
            for (String s : linesToConvert){
                if (!s.contentEquals(xmlFileStart) && !s.contentEquals(xmlFileEnd) && !s.contentEquals("") && !s.contains("translatable=\"false\">")){
                    String tmp;

                    if (s.startsWith("<?xml") || s.startsWith("<!--")){
                        temp = true;
                        tmp = s.replace("<?xml", "#").replace("<!--", "#");
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = tmp.replace("?>", "").replace("-->", "");
                        }
                    }else if (temp){
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = "#" + s;
                        }else{
                            temp = true;
                            tmp = "#" + s;
                        }
                    }else{
                        tmp = s.replace("    ", "").replace("\t", "").replace("  ", "").replace(xmlFileStart, "").replace(xmlLineStart, "").replace(xmlLineMiddle, "=").replace(xmlLineEnd, "");
                    }
                    convertedLines.add(tmp.replace("\'", "'"));
                }
            }

            for (String s : convertedLines){
                properties.setProperty(s.split("=")[0], s.split("=")[1]);
            }
            properties.store(outputStream, null);

            conversionProgressListener.onConversionFinished("File Converted! Saved as \"" + convertedFile.getName() + "\" at " + convertedFile.getAbsolutePath());

        }catch (Exception e){
            conversionProgressListener.onProgressUpdate("Something went wrong!");
        }
    }


    private static void detailedConversionToXml(File inputFile, File outputFile){
        detailedConversionProgressListener.onProgressUpdate("Converting .properties file to strings.xml...");

        detailedConversionProgressListener.onDetailedProgressUpdate("Configuring tools...");
        String fileStart = "<resources>";
        String lineStart = "    <string name=\"";
        String lineMiddle = "\">";
        String lineEnd = "</string>";
        String fileEnd = "</resources>";

        detailedConversionProgressListener.onDetailedProgressUpdate("Creating new file with the name \"strings.xml\"...");
        File convertedFile = outputFile;

        Properties properties = new Properties();
        FileInputStream inputStream;
        String keysString = "";
        String[] keysArray;
        try {
            inputStream = new FileInputStream(inputFile);
            properties.load(inputStream);

            detailedConversionProgressListener.onDetailedProgressUpdate("Getting key names from properties file...");
            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                if (keysString.contentEquals("")){
                    keysString = (String) e.nextElement();
                }else {
                    keysString = keysString + ";#;" + (String) e.nextElement();
                }
            }

            keysArray = keysString.split(";#;");


            ArrayList<String> linesToConvert = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating lines that exist in the raw file.");
            for (String s : keysArray){
                linesToConvert.add(s + "=" + properties.getProperty(s));
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating list for converted lines...");
            ArrayList<String> convertedLines = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Adding start of file...");
            convertedLines.add(fileStart);

            detailedConversionProgressListener.onDetailedProgressUpdate("Converting lines...");
            for (String s : linesToConvert){
                String tmp = lineStart + s.split("=")[0].replace(".", "") + lineMiddle + s.split("=")[1] + lineEnd;
                convertedLines.add(tmp);
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Converted lines. Adding file ending...");
            convertedLines.add(fileEnd);

            detailedConversionProgressListener.onDetailedProgressUpdate("Writing new file...");
            Files.write(convertedFile.toPath().toAbsolutePath(), convertedLines, StandardCharsets.UTF_8);

            detailedConversionProgressListener.onProgressUpdate("File Converted! Saved as \"" + convertedFile.getName() + "\" at " + convertedFile.getAbsolutePath());
            detailedConversionProgressListener.onDetailedProgressUpdate("");
        }catch (Exception e){
            e.printStackTrace();
            detailedConversionProgressListener.onProgressUpdate("Something went wrong!");
            detailedConversionProgressListener.onDetailedProgressUpdate(e.getLocalizedMessage());
        }
    }

    private static void detailedConversionToProperties(File inputFile, File outputFile){
        detailedConversionProgressListener.onProgressUpdate("Converting .xml file to .properties");

        String xmlFileStart = "<resources>";
        String xmlLineStart = "<string name=\"";
        String xmlLineMiddle = "\">";
        String xmlLineEnd = "</string>";
        String xmlFileEnd = "</resources>";

        detailedConversionProgressListener.onDetailedProgressUpdate("Creating new file with the name \"java.properties\"...");
        File convertedFile = outputFile;

        try{
            Properties properties = new Properties();
            FileOutputStream outputStream = new FileOutputStream(convertedFile);


            detailedConversionProgressListener.onDetailedProgressUpdate("Reading lines to convert...");
            List<String> linesToConvert =  Files.readAllLines(inputFile.toPath().toAbsolutePath(), StandardCharsets.UTF_8);

            detailedConversionProgressListener.onDetailedProgressUpdate("Creating list for converted lines...");
            ArrayList<String> convertedLines = new ArrayList<String>();

            detailedConversionProgressListener.onDetailedProgressUpdate("Converting lines...");
            boolean temp = false;
            for (String s : linesToConvert){
                if (!s.contentEquals(xmlFileStart) && !s.contentEquals(xmlFileEnd) && !s.contentEquals("") && !s.contains("translatable=\"false\">")){
                    String tmp;

                    if (s.startsWith("<?xml") || s.startsWith("<!--")){
                        temp = true;
                        tmp = s.replace("<?xml", "#").replace("<!--", "#");
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = tmp.replace("?>", "").replace("-->", "");
                        }
                    }else if (temp){
                        if (s.endsWith("?>") || s.endsWith("-->")){
                            temp = false;
                            tmp = "#" + s;
                        }else{
                            temp = true;
                            tmp = "#" + s;
                        }
                    }else{
                        tmp = s.replace("    ", "").replace("\t", "").replace("  ", "").replace(xmlFileStart, "").replace(xmlLineStart, "").replace(xmlLineMiddle, "=").replace(xmlLineEnd, "");
                    }
                    convertedLines.add(tmp);
                }
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Writing new Properties...");
            for (String s : convertedLines){
                if (!s.startsWith("#")){
                    properties.setProperty(s.split("=")[0], s.split("=")[1]);
                }
            }

            detailedConversionProgressListener.onDetailedProgressUpdate("Saving new file...");
            properties.store(outputStream, null);

            detailedConversionProgressListener.onProgressUpdate("File Converted! Saved as \"" + convertedFile.getName() + "\" at " + convertedFile.getAbsolutePath());
            detailedConversionProgressListener.onDetailedProgressUpdate("");

        }catch (Exception e){
            e.printStackTrace();
            detailedConversionProgressListener.onProgressUpdate("Something went wrong!");
            detailedConversionProgressListener.onDetailedProgressUpdate(e.getMessage());
        }
    }
}
